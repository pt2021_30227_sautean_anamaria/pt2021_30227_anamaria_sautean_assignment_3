package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.ClientNameValidator;
import bll.validators.EmailValidator;
import bll.validators.ClientAgeValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */

/**
 * Clasa care apeleaza functiile din AbstractDao
 */

public class ClientBLL {

	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new ClientAgeValidator());
		validators.add(new ClientNameValidator());

		clientDAO = new ClientDAO();
	}

	public Client findStudentById(int id) {
		Client st = clientDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The student with id =" + id + " was not found!");
		}
		return st;
	}

	public List<Client> findAll(){
		List<Client> st = clientDAO.findAll();
		if (st == null) {
			throw new NoSuchElementException("Nu Exista");
		}
		return st;
	}

	public void  insert(Client c) throws Exception{
		for (Validator<Client> clientValidator : validators) {
			clientValidator.validate(c);
		}
		clientDAO.insert(c);
	}

	public void delete(int id){
		Client c = new Client();
		clientDAO.delete(c, id);
	}

	public void update(int id,Client c){
		clientDAO.update(id,c);
	}

}
