package bll;

import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.Orders;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */
/**
 * Clasa care apeleaza functiile din AbstractDao
 */
public class OrderBLL {
    private OrderDAO orderDao;
    List<Validator> validator=new ArrayList<>();

    public OrderBLL() {
        orderDao = new OrderDAO();
        validator.add(new QuantityValidator());
    }

    public Orders findStudentById(int id) {
        Orders st = orderDao.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The Produs with id =" + id + " was not found!");
        }
        return st;
    }

    public List<Orders> findAll(){
        List<Orders> st = orderDao.findAll();
        if (st == null) {
            throw new NoSuchElementException("Nu Exista");
        }
        return st;
    }

    public void  insert(Orders o) throws Exception {
        orderDao.insert(o);
    }

    public void delete(int id){
        Orders o = new Orders();
        orderDao.delete(o, id);
    }

    public void update(int id, Orders o){
        orderDao.update(id,o);
    }
}
