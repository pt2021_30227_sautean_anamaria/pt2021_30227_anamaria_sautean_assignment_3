package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.ProdusPretValidator;
import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.ProdusDAO;
import model.Produs;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */
/**
 * Clasa care apeleaza functiile din AbstractDao
 */

public class ProdusBLL {
	private ProdusDAO produsDAO;
	List<Validator> validator=new ArrayList<>();

	public ProdusBLL() {
		produsDAO = new ProdusDAO();
		validator.add(new ProdusPretValidator());
	}
	
	public Produs findProdusById(int id) {
		Produs st = produsDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Produs with id =" + id + " was not found!");
		}
		return st;
	}

	public List<Produs> findAll(){
		List<Produs> st = produsDAO.findAll();
		if (st == null) {
			throw new NoSuchElementException("Nu Exista");
		}
		return st;
	}

	public void  insert(Produs p) throws Exception{
		for (Validator validator1 : validator) {
			try {
				validator1.validate(p);
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
		}
		produsDAO.insert(p);
	}

	public void delete(int id){
		Produs p = new Produs();
		produsDAO.delete(p, id);
	}

	public void update(int id,Produs p){
		produsDAO.update(id,p);
	}
}
