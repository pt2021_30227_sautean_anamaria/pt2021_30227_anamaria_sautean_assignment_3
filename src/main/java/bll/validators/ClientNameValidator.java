package bll.validators;

import model.Client;

import java.util.Objects;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Clasa de validare a numelui clientului
 */
public class ClientNameValidator implements Validator<Client> {
    @Override
    public void validate(Client client) throws Exception {
        if(Objects.equals(client.getName(), "") || client.getName() == null){
            throw new Exception("Nume Invalid!");
        }
    }
}
