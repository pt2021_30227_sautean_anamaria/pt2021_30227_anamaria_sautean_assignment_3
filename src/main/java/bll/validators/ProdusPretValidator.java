package bll.validators;
import model.Produs;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Clasa de validare a pretului produsului
 */
public class ProdusPretValidator implements Validator <Produs>{

    public void validate(Produs t)
    {
        if(t.getPrice() < 0)
        {
            throw new IllegalArgumentException("The product price is not valid!");
        }
    }
}
