package bll.validators;
import model.Orders;
import model.Produs;
import bll.ProdusBLL;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Clasa de validare a cantitatii produsului
 */
public class QuantityValidator implements Validator<Orders>{

    public void validate(Orders order) {
        ProdusBLL productBLl = new ProdusBLL();
        Produs product = productBLl.findProdusById(order.getIdProdus());

        if(product == null )
            throw new IllegalArgumentException("No such product in the store");
        if(product.getQuantity() < order.getCantitate()) {
            throw new IllegalArgumentException("Too much quantity asked");
        }
    }
}
