package bll.validators;

/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Clasa de validare
 */
public interface Validator<T> {

	public void validate(T t) throws Exception;
}
