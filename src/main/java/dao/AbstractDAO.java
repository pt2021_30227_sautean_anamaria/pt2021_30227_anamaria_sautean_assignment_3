package dao;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	/**
	 * Constructorul clasei abstractDao
	 * */
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/**
	 * Metoda de creare a interogarii Select
	 * @param field
	 * @return un string
	 */
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	/**
	 * Metoda de creare a interogarii FindALL
	 * @return un string
	 */
	private String createFindAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}

	/**
	 * Functie ce returneaza continutul unei tabele
	 * @return lista din tabela
	 */
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createFindAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Metoda ce cauta un element in tabel dupa un id
	 * @param id
	 * @return elementul
	 */
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Converteste datele din tabel in obiecte Java
	 * @param resultSet
	 * @return lista de obiecte
	 */
	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();
		Constructor[] ctors = type.getDeclaredConstructors();
		Constructor ctor = null;
		for (int i = 0; i < ctors.length; i++) {
			ctor = ctors[i];
			if (ctor.getGenericParameterTypes().length == 0)
				break;
		}
		try {
			while (resultSet.next()) {
				ctor.setAccessible(true);
				T instance = (T)ctor.newInstance();
				for (Field field : type.getDeclaredFields()) {
					String fieldName = field.getName();
					Object value = resultSet.getObject(fieldName);
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Metoda de inserare intr-un tabel
	 * @param t
	 * @return elemntul inserat
	 * @throws Exception
	 */
	public T insert(T t) throws Exception {
		Connection connection;
		PreparedStatement statement=null;
		String querry="insert into "+t.getClass().getSimpleName()+" value (?,?,?,?)";

		connection = ConnectionFactory.getConnection();

		try{
			int i=1;
			statement = connection.prepareStatement(querry);
			for (Field field : t.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				statement.setObject(i,field.get(t));
				i++;
			}
			statement.executeUpdate();

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

		return t;
	}
	/**
	 * Metoda de editare intr-un tabel
	 * @param id
	 * @param t
	 * @return elemntul editat
	 * @throws Exception
	 */
	public T update(int id,T t) {
		try {
			findById(id);
			Connection connection=ConnectionFactory.getConnection();
			PreparedStatement statement=null;
			String querry = "Update " +t.getClass().getSimpleName()+ " set ";
			for (Field declaredField : t.getClass().getDeclaredFields()) {
				querry+=declaredField.getName()+"=? ,";
			}
			querry=querry.substring(0,querry.length()-1);
			querry+=" where id=?";
			statement=connection.prepareStatement(querry);
			int i=1;
			for (Field declaredField : t.getClass().getDeclaredFields()) {
				declaredField.setAccessible(true);
				statement.setObject(i,declaredField.get(t));
				i++;
			}
			statement.setObject(5,id);

			statement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}
	/**
	 * Metoda de stergere dintr-un tabel
	 * @param t
	 * @param id
	 * @throws Exception
	 */
	public void delete(T t, int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = "DELETE FROM " + t.getClass().getSimpleName() + " WHERE id = ?";
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}

	}
}
