package dao;

import connection.ConnectionFactory;
import model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */

public class ClientDAO extends AbstractDAO<Client> {

	// uses basic CRUD methods from superclass

	// TODO: create only client specific queries

}
