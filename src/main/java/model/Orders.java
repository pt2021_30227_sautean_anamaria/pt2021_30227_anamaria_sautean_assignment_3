package model;

/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */
/**
 * Clasa Orders contine atributele specifice unei comenzi si gettere-ele si settere-le corespunzatoare.
 * */
public class Orders {
	private int id;
	private int idClient;
	private int idProdus;
	private int Cantitate;



public Orders() {
}

public Orders(int id, int idClient, int idProdus, int Cantitate) {
	super();
	this.id = id;
	this.idClient = idClient;
	this.idProdus = idProdus;
	this.Cantitate = Cantitate;
}

public Orders(int idClient, int idProdus, int Cantitate){
	this.idClient = idClient;
	this.idProdus = idProdus;
	this.Cantitate = Cantitate;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getIdClient() {
	return idClient;
}

public void setIdClient(int idClient) {
	this.idClient = idClient;
}

public int getIdProdus() {
	return idProdus;
}

public void setIdProdus(int idProdus) {
	this.idProdus = idProdus;
}

public int getCantitate() {
	return Cantitate;
}

public void setCantitate(int cantitate) {
	Cantitate = cantitate;
}
}
