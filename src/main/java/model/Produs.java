package model;

/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 */
/**
 * Clasa Client contine atributele specifice unui produs si gettere-ele si settere-le corespunzatoare.
 * */
public class Produs {
   
	private int id;
	private String name;
	private int price;
	private int quantity;

	public Produs() {
	}

	public Produs(int id, String name, int price, int quantity) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public Produs(String name, int price, int quantity){
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
