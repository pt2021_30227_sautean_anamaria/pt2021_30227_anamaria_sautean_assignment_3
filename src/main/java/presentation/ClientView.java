package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ClientView extends JFrame {
    private JButton addClientButton=new JButton("Add");
    private JButton deleteClientButton=new JButton("Delete");
    private JButton updateClientButton=new JButton("Update");
    private JButton getClientButton=new JButton("View All");
    private JButton findClientButton=new JButton("Find By Id");

    private JTextField idField=new JTextField();
    private JTextField numeField=new JTextField();
    private JTextField mailField=new JTextField();
    private JTextField ageField=new JTextField();

    JTable clients=new JTable();
    JPanel panel;

    public ClientView() {
        JPanel tabel = new JPanel();
        tabel.setLayout(new FlowLayout());
        tabel.add(clients);

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));

        butoane.add(new JLabel("ID:"));
        idField.setSize(20,10);
        butoane.add(idField);
        butoane.add(new JLabel("Nume:"));
        numeField.setSize(20,10);
        butoane.add(numeField);
        butoane.add(new JLabel("Mail:"));
        mailField.setSize(20,10);
        butoane.add(mailField);
        butoane.add(new JLabel("Varsta:"));
        ageField.setSize(20,10);
        butoane.add(ageField);

        butoane.add(addClientButton);
        butoane.add(deleteClientButton);
        butoane.add(updateClientButton);
        butoane.add(getClientButton);
        butoane.add(findClientButton);

        JPanel frame = new JPanel();
        frame.setLayout(new BoxLayout(frame, BoxLayout.X_AXIS));
        frame.add(tabel);
        frame.add(butoane);

        this.setContentPane(frame);
        this.setTitle("Client");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        panel=frame;
    }

    public void setClients(JTable clients) {
        this.clients.setModel(clients.getModel());
    }

    public void setGetClientButtonListener(ActionListener listener) {
        getClientButton.addActionListener(listener);
    }

    public void setAddClientButtonListener(ActionListener listener){
        addClientButton.addActionListener(listener);
    }

    public void setDeleteClientButtonListener(ActionListener listener){
        deleteClientButton.addActionListener(listener);
    }

    public void setUpdateClientButtonListener(ActionListener listener){
        updateClientButton.addActionListener(listener);
    }

    public void setFindClientButtonListener(ActionListener listener){
        findClientButton.addActionListener(listener);
    }

    public String getIdField(){
        return idField.getText();
    }

    public String getNumeField(){
        return numeField.getText();
    }

    public String getMailField(){
        return mailField.getText();
    }

    public String getAgeField(){
        return ageField.getText();
    }

}
