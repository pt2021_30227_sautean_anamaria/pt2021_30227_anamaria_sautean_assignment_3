package presentation;

import bll.ClientBLL;
import bll.ProdusBLL;
import com.mysql.cj.x.protobuf.MysqlxCrud;
import model.Client;

import bll.ClientBLL;
import bll.ProdusBLL;
import bll.OrderBLL;
import model.Orders;
import model.Produs;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

class ReflectionProps{
    public static String[] retrieveProperties(Object object) {
        int i=0;
        String[] headers=new String[4];
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;

            try {
                value = field.get(object);
                headers[i]=field.getName();
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return headers;
    }
    public static Object[] retrieveElements(Object object) {
        int i=0;
        Object[] values=new Object[4];
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;

            try {
                value = field.get(object);
                values[i]=value;
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return values;
    }
}

public class Controller {
    private View view;
    private ClientView clientView;
    private ProdusView produsView;
    private OrderView orderView;

    private ClientBLL clientBLL=new ClientBLL();
    private ProdusBLL produsBLL=new ProdusBLL();
    private OrderBLL orderBLL=new OrderBLL();

    public Controller(View view){
        this.view = view;
        this.view.addClientListener(new ClientListener());
        this.view.addProdusListener(new ProdusListener());
        this.view.addOrderListener(new OrderListener());
    }

    public Controller(ClientView clientView){
        this.clientView = clientView;
        this.clientView.setGetClientButtonListener(new GetClients());
        this.clientView.setAddClientButtonListener(new AddClient());
        this.clientView.setDeleteClientButtonListener(new DeleteClient());
        this.clientView.setUpdateClientButtonListener(new UpdateClient());
        this.clientView.setFindClientButtonListener(new FindClient());

    }

    public Controller(ProdusView produsView){
        this.produsView = produsView;
        this.produsView.setAddProdusButtonListener(new AddProdus());
        this.produsView.setGetProdusButtonListener(new GetProdus());
        this.produsView.setFindProdusButtonListener(new FindProdus());
        this.produsView.setDeleteProdusButtonListener(new DeleteProdus());
        this.produsView.setUpdateProdusButtonListener(new UpdateProdus());
    }

    public Controller(OrderView orderView){
        this.orderView = orderView;
        this.orderView.addOrderListener(new AddOrder());
        this.orderView.editOrderListener(new EditOrder());
        this.orderView.deleteOrderListener(new DeleteOrder());
        this.orderView.viewAllOrdersListener(new ViewAllOrders());
    }

    private class ClientListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ClientView clientView = new ClientView();
            clientView.setVisible(true);
            clientView.setSize(500, 500);
            Controller controller = new Controller(clientView);
        }
    }

    private class ProdusListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ProdusView produsView = new ProdusView();
            produsView.setVisible(true);
            produsView.setSize(500, 500);
            Controller controller = new Controller(produsView);
        }
    }

    private class OrderListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            OrderView orderView = new OrderView();
            orderView.setVisible(true);
            orderView.setSize(500, 500);
            Controller controller = new Controller(orderView);
        }
    }

    private class GetClients implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            List<Client> clientList=clientBLL.findAll();
            String[] columnNames={"Id","Nume","Mail","Varsta"};
            Object[][] clients=new Object[clientList.size()+1][4];
            clients[0]=ReflectionProps.retrieveProperties(clientList.get(0));
            int i=1;
            for (Client client : clientList) {
                clients[i]=ReflectionProps.retrieveElements(clientList.get(i-1));
                i++;
            }

            JTable table=new JTable(clients,columnNames);
            table.setVisible(true);
            clientView.setClients(table);
        }
    }

    private class AddClient implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String id=clientView.getIdField();
            String name=clientView.getNumeField();
            String mail=clientView.getMailField();
            String age=clientView.getAgeField();
            try {
                if(id==null|| Objects.equals(name, null) ||mail==""||age==null)
                    throw new Exception("Date incorecte!");
                clientBLL.insert(new Client(Integer.parseInt(id), name, mail, Integer.parseInt(age)));
                JOptionPane.showMessageDialog(clientView,"Added Successfully");
                List<Client> clientList=clientBLL.findAll();
                String[] columnNames={"Id","Nume","Mail","Varsta"};
                Object[][] clients=new Object[clientList.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(clientList.get(0));
                int i=1;
                for (Client client : clientList) {
                    clients[i]=ReflectionProps.retrieveElements(clientList.get(i-1));
                    i++;
                }

                JTable table=new JTable(clients,columnNames);
                table.setVisible(true);
                clientView.setClients(table);
            } catch (Exception err) {
                JOptionPane.showMessageDialog(clientView,err.getMessage());
            }

        }
    }

    private class DeleteClient implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String id = clientView.getIdField();
                if(id==null)
                    throw new Exception("Date incorecte!");
                clientBLL.delete(Integer.parseInt(id));
                JOptionPane.showMessageDialog(clientView, "Deleted Successfully");
                List<Client> clientList = clientBLL.findAll();
                String[] columnNames = {"Id", "Nume", "Mail", "Varsta"};
                Object[][] clients=new Object[clientList.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(clientList.get(0));
                int i=1;
                for (Client client : clientList) {
                    clients[i]=ReflectionProps.retrieveElements(clientList.get(i-1));
                    i++;
                }
                JTable table = new JTable(clients, columnNames);
                table.setVisible(true);
                clientView.setClients(table);
            }catch (Exception err){
                JOptionPane.showMessageDialog(clientView,err.getMessage());
            }
        }
    }

    private class UpdateClient implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Integer id = Integer.parseInt(clientView.getIdField());
                String nume = clientView.getNumeField();
                String mail = clientView.getMailField();
                Integer age = Integer.parseInt(clientView.getAgeField());
                if(id==null||nume==null||mail==""||age==null)
                    throw new Exception("Date incorecte!");
                clientBLL.update(id,new Client(id,nume,mail,age));
                JOptionPane.showMessageDialog(clientView, "Update Successfully");
                List<Client> clientList = clientBLL.findAll();
                String[] columnNames = {"Id", "Nume", "Mail", "Varsta"};
                Object[][] clients=new Object[clientList.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(clientList.get(0));
                int i=1;
                for (Client client : clientList) {
                    clients[i]=ReflectionProps.retrieveElements(clientList.get(i-1));
                    i++;
                }

                JTable table = new JTable(clients, columnNames);
                table.setVisible(true);
                clientView.setClients(table);
            }catch (Exception err){
                JOptionPane.showMessageDialog(clientView,err.getMessage());
            }
        }
    }

    private class FindClient implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Integer id=Integer.parseInt(clientView.getIdField());
                if(id==null)
                    throw new Exception("Date incorecte!");
                Client c=clientBLL.findStudentById(id);
                JOptionPane.showMessageDialog(clientView, "Found!");
                List<Client> clientList = clientBLL.findAll();
                String[] columnNames = {"Id", "Nume", "Mail", "Varsta"};
                Object[][] clients=new Object[clientList.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(clientList.get(0));
                int i=1;

                clients[i]=ReflectionProps.retrieveElements(c);

                JTable table = new JTable(clients, columnNames);
                table.setVisible(true);
                clientView.setClients(table);
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(clientView, exception.getMessage());
            }
        }
    }

    private class AddProdus implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Integer id=Integer.parseInt(produsView.getIdField());
                String nume=produsView.getNumeField();
                Integer pret=Integer.parseInt(produsView.getPriceField());
                Integer cantitate=Integer.parseInt(produsView.getQuantityField());
                produsBLL.insert(new Produs(id,nume,pret,cantitate));
                JOptionPane.showMessageDialog(clientView, "Produs added!");

                List<Produs> produses=produsBLL.findAll();
                Object[][] clients=new Object[produses.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(produses.get(0));
                int i=1;
                for (Produs client : produses) {
                    clients[i]=ReflectionProps.retrieveElements(produses.get(i-1));
                    i++;
                }
                JTable table = new JTable(clients, clients[0]);
                table.setVisible(true);
                produsView.setProduse(table);
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(clientView, exception.getMessage());
            }
        }
    }

    private class GetProdus implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            List<Produs> produses=produsBLL.findAll();
            if(produses.size()==0)
            {
                JTable table = new JTable();
                table.setVisible(true);
                produsView.setProduse(table);
                return;
            }
            Object[][] clients=new Object[produses.size()+1][4];
            clients[0]=ReflectionProps.retrieveProperties(produses.get(0));
            int i=1;
            for (Produs client : produses) {
                clients[i]=ReflectionProps.retrieveElements(produses.get(i-1));
                i++;
            }
            JTable table = new JTable(clients, clients[0]);
            table.setVisible(true);
            produsView.setProduse(table);
        }
    }

    private class FindProdus implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Integer id = Integer.parseInt(produsView.getIdField());
                Produs p = produsBLL.findProdusById(id);
                int n = produsBLL.findAll().size();
                Object[][] produses = new Object[n][4];
                produses[0] = ReflectionProps.retrieveProperties(p);
                produses[1] = ReflectionProps.retrieveElements(p);

                JTable table = new JTable(produses, produses[0]);
                table.setVisible(true);
                produsView.setProduse(table);
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(clientView, exception.getMessage());
            }
        }
    }

    private class DeleteProdus implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Integer id=Integer.parseInt(produsView.getIdField());
                produsBLL.delete(id);
                List<Produs> produses=produsBLL.findAll();
                if(produses.size()==0)
                {
                    JTable table = new JTable();
                    table.setVisible(true);
                    produsView.setProduse(table);
                    return;
                }
                Object[][] clients=new Object[produses.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(produses.get(0));
                int i=1;
                for (Produs client : produses) {
                    clients[i]=ReflectionProps.retrieveElements(produses.get(i-1));
                    i++;
                }
                JTable table = new JTable(clients, clients[0]);
                table.setVisible(true);
                produsView.setProduse(table);
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(clientView, exception.getMessage());
            }

        }
    }

    private class UpdateProdus implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            try {
                Integer id=Integer.parseInt(produsView.getIdField());
                String nume=produsView.getNumeField();
                Integer pret=Integer.parseInt(produsView.getPriceField());
                Integer cantitate=Integer.parseInt(produsView.getQuantityField());
                produsBLL.update(id,new Produs(id,nume,pret,cantitate));
                List<Produs> produses=produsBLL.findAll();
                Object[][] clients=new Object[produses.size()+1][4];
                clients[0]=ReflectionProps.retrieveProperties(produses.get(0));
                int i=1;
                for (Produs client : produses) {
                    clients[i]=ReflectionProps.retrieveElements(produses.get(i-1));
                    i++;
                }
                JTable table = new JTable(clients, clients[0]);
                table.setVisible(true);
                produsView.setProduse(table);
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(clientView, exception.getMessage());
            }
        }
    }
    private class AddOrder implements ActionListener {
        public void writeToFile(){
            try {
                FileWriter myWriter = new FileWriter("Comenzi.txt");
                for (Orders orders : orderBLL.findAll()) {
                    Produs p = produsBLL.findProdusById(orders.getIdProdus());
                    Client c = clientBLL.findStudentById(orders.getIdClient());
                    myWriter.write(c.getName() + " : " + p.getName() + " in cantitate de " + orders.getCantitate() + " in valoare totala de: " + orders.getCantitate() * p.getPrice()+"\n");
                }

                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.out.println("An error occurred.");
                JOptionPane.showMessageDialog(clientView, e.getMessage());
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String id = orderView.getIdField();
            String idClient = orderView.getIdClientField();
            String idProdus = orderView.getIdProdusField();
            String cantitate = orderView.getCantitateField();
            try {
                if(id==null|| Objects.equals(idClient, null) ||idProdus==""||cantitate==null)
                    throw new Exception("Date incorecte!");

                Produs p =produsBLL.findProdusById(Integer.parseInt(idProdus));
                if(p==null || p.getQuantity()<Integer.parseInt(cantitate)){
                    JOptionPane.showMessageDialog(orderView,"Cantitate prea mare");
                    return;
                }
                    orderBLL.insert(new Orders(Integer.parseInt(id), Integer.parseInt(idClient), Integer.parseInt(idProdus), Integer.parseInt(cantitate)));
                    produsBLL.update(p.getId(), new Produs(p.getId(), p.getName(), p.getPrice(), p.getQuantity() - Integer.parseInt(cantitate)));


                writeToFile();
                JOptionPane.showMessageDialog(orderView, "Added Successfully");
                List<Orders> orderList=orderBLL.findAll();
                Object[][] orders=new Object[orderList.size()+1][4];
                orders[0]=ReflectionProps.retrieveProperties(orderList.get(0));
                int i=1;
                for (Orders order : orderList) {
                    orders[i]=ReflectionProps.retrieveElements(orderList.get(i-1));
                    i++;
                }
                String[] columnNames = {"ID", "ID Client", "ID Produs", "Cantitate"};
                JTable table=new JTable(orders,columnNames);
                table.setVisible(true);
                orderView.setTable(table);
            } catch (Exception err) {
                JOptionPane.showMessageDialog(orderView,err.getMessage());
            }
        }
    }

    private class EditOrder implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Integer id = Integer.parseInt(orderView.getIdField());
                Integer idClient = Integer.parseInt(orderView.getIdClientField());
                Integer idProdus = Integer.parseInt(orderView.getIdProdusField());
                Integer cantitate = Integer.parseInt(orderView.getCantitateField());
                if(id==null||idClient==null||idProdus==null||cantitate==null)
                    throw new Exception("Date incorecte!");
                Produs initial=produsBLL.findProdusById(idProdus);
                Orders order1=orderBLL.findStudentById(id);
                initial.setQuantity(initial.getQuantity()+order1.getCantitate());
                if(cantitate<initial.getQuantity())
                {
                    orderBLL.delete(id);
                    produsBLL.update(idProdus,new Produs(initial.getId(),initial.getName(),initial.getPrice(),initial.getQuantity()-cantitate));
                    orderBLL.insert(new Orders(id,idClient,idProdus,cantitate));
                }

                orderBLL.update(id,new Orders(id,idClient,idProdus,cantitate));
                JOptionPane.showMessageDialog(orderView, "Update Successfully");
                List<Orders> orderList = orderBLL.findAll();
                String[] columnNames = {"Id", "ID Client", "ID Produs", "Cantitate"};
                Object[][] orders=new Object[orderList.size()+1][4];
                orders[0]=ReflectionProps.retrieveProperties(orderList.get(0));
                int i=1;
                for (Orders order : orderList) {
                    orders[i]=ReflectionProps.retrieveElements(orderList.get(i-1));
                    i++;
                }

                JTable table = new JTable(orders, columnNames);
                table.setVisible(true);
                orderView.setTable(table);
            }catch (Exception err){
                JOptionPane.showMessageDialog(orderView,err.getMessage());
            }
        }
    }

    private class DeleteOrder implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String id = orderView.getIdField();
                if(id==null)
                    throw new Exception("Date incorecte!");
                Orders o = orderBLL.findStudentById(Integer.parseInt(id));
                Produs p= produsBLL.findProdusById(o.getIdProdus());
                orderBLL.delete(Integer.parseInt(id));
                produsBLL.update(Integer.parseInt(id),new Produs(p.getId(),p.getName(),p.getPrice(),p.getQuantity()+o.getCantitate()));
                JOptionPane.showMessageDialog(orderView, "Deleted Successfully");
                List<Orders> orderList = orderBLL.findAll();
                if(orderList.size()==0)
                {
                    JTable table = new JTable();
                    table.setVisible(true);
                    orderView.setTable(table);
                    return;
                }
                String[] columnNames = {"Id", "ID CLient", "ID Produs", "Cantitate"};
                Object[][] orders=new Object[orderList.size()+1][4];
                orders[0]=ReflectionProps.retrieveProperties(orderList.get(0));
                int i=1;
                for (Orders order : orderList) {
                    orders[i]=ReflectionProps.retrieveElements(orderList.get(i-1));
                    i++;
                }
                JTable table = new JTable(orders, columnNames);
                table.setVisible(true);
                orderView.setTable(table);
            }catch (Exception err){
                JOptionPane.showMessageDialog(orderView,err.getMessage());
            }
        }
    }

    private class ViewAllOrders implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            List<Orders> orderList=orderBLL.findAll();
            if(orderList.size()==0)
            {

                JTable table = new JTable();
                table.setVisible(true);
                orderView.setTable(table);
                return;
            }
            String[] columnNames={"Id","ID CLient","ID Produs","Cantitate"};
            Object[][] orders=new Object[orderList.size()+1][4];
            orders[0]=ReflectionProps.retrieveProperties(orderList.get(0));
            int i=1;
            for (Orders order : orderList) {
                orders[i]=ReflectionProps.retrieveElements(orderList.get(i-1));
                i++;
            }

            JTable table=new JTable(orders,columnNames);
            table.setVisible(true);
            orderView.setTable(table);
        }
    }
}
