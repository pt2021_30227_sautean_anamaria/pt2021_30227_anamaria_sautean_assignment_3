package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Pagina corespunzatoare comenzii
 */
public class OrderView  extends JFrame {
    private JTextField idField = new JTextField();
    private JTextField idClientField = new JTextField();
    private JTextField idProdusField = new JTextField();
    private JTextField cantitateField = new JTextField();

    private JButton addOrder = new JButton("Add");
    private JButton delete = new JButton("Delete");
    private JButton edit = new JButton("Edit");
    private JButton viewAll = new JButton("View All");

    private JTable table = new JTable();

    public OrderView(){
        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));
        butoane.add(new JLabel("ID"));
        butoane.add(idField);
        butoane.add(new JLabel("ID Client"));
        butoane.add(idClientField);
        butoane.add(new JLabel("ID Produs"));
        butoane.add(idProdusField);
        butoane.add(new JLabel("Cantitate"));
        butoane.add(cantitateField);

        butoane.add(addOrder);
        butoane.add(delete);
        butoane.add(edit);
        butoane.add(viewAll);

        JPanel tabel = new JPanel();
        tabel.setLayout(new FlowLayout());
        tabel.add(table);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(tabel);
        panel.add(butoane);

        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle("Order");
        this.setVisible(true);
    }

    public void setTable(JTable table) {
        this.table.setModel(table.getModel());
    }

    public String getIdField() {
        return idField.getText();
    }

    public String getIdClientField() {
        return idClientField.getText();
    }

    public String getIdProdusField() {
        return idProdusField.getText();
    }

    public String getCantitateField() {
        return cantitateField.getText();
    }

    public void addOrderListener(ActionListener l){
        this.addOrder.addActionListener(l);
    }

    public void deleteOrderListener(ActionListener l){
        this.delete.addActionListener(l);
    }

    public void editOrderListener(ActionListener l){
        this.edit.addActionListener(l);
    }

    public void viewAllOrdersListener(ActionListener l){
        this.viewAll.addActionListener(l);
    }

}
