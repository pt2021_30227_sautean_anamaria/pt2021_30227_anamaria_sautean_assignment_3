package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Pagina corespunzatoare produsului
 */
public class ProdusView  extends JFrame {
    private JButton addProdusButton=new JButton("Add");
    private JButton deleteProdusButton=new JButton("Delete");
    private JButton updateProdusButton=new JButton("Update");
    private JButton getProdusButton=new JButton("View All");
    private JButton findProdusButton=new JButton("Find By Id");

    private JTextField idField=new JTextField();
    private JTextField numeField=new JTextField();
    private JTextField priceField=new JTextField();
    private JTextField quantityField=new JTextField();

    JTable produse = new JTable();
    JPanel panel;

    public ProdusView(){
       JPanel tabel = new JPanel();
       tabel.setLayout(new FlowLayout());
       tabel.add(produse);

       JPanel butoane = new JPanel();
       butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));

        butoane.add(new JLabel("ID:"));
        idField.setSize(20,10);
        butoane.add(idField);
        butoane.add(new JLabel("Nume:"));
        numeField.setSize(20,10);
        butoane.add(numeField);
        butoane.add(new JLabel("Pret:"));
        priceField.setSize(20,10);
        butoane.add(priceField);
        butoane.add(new JLabel("Cantitate:"));
        quantityField.setSize(20,10);
        butoane.add(quantityField);

        butoane.add(addProdusButton);
        butoane.add(deleteProdusButton);
        butoane.add(updateProdusButton);
        butoane.add(getProdusButton);
        butoane.add(findProdusButton);

        JPanel frame = new JPanel();
        frame.setLayout(new BoxLayout(frame, BoxLayout.X_AXIS));
        frame.add(tabel);
        frame.add(butoane);

        this.setContentPane(frame);
        this.setTitle("Produs");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        panel=frame;

    }

    public void setProduse(JTable clients) {
        this.produse.setModel(clients.getModel());
    }

    public void setGetProdusButtonListener(ActionListener listener) {
        getProdusButton.addActionListener(listener);
    }

    public void setAddProdusButtonListener(ActionListener listener){
        addProdusButton.addActionListener(listener);
    }

    public void setDeleteProdusButtonListener(ActionListener listener){
        deleteProdusButton.addActionListener(listener);
    }

    public void setUpdateProdusButtonListener(ActionListener listener){
        updateProdusButton.addActionListener(listener);
    }

    public void setFindProdusButtonListener(ActionListener listener){
        findProdusButton.addActionListener(listener);
    }

    public String getIdField(){
        return idField.getText();
    }

    public String getNumeField(){
        return numeField.getText();
    }

    public String getPriceField(){
        return priceField.getText();
    }

    public String getQuantityField(){
        return quantityField.getText();
    }
}
