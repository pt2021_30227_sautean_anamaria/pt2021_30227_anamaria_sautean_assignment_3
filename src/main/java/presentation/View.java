package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
/**
 * @Author: Anamaria Sautean
 * @Since: Sep 01, 2021
 * Pagina principala a aplicatiei
 */

public class View extends JFrame {
    private JButton client = new JButton("Client");
    private JButton order = new JButton("Order");
    private JButton produs = new JButton("Produs");

    public View(){
        JPanel titlu = new JPanel();
        titlu.setLayout(new FlowLayout());
        titlu.add(new JLabel("Order management"));

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.X_AXIS));
        butoane.add(client);
        butoane.add(produs);
        butoane.add(order);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(titlu);
        panel.add(butoane);

        this.setContentPane(panel);
        this.setTitle("Order management");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void addClientListener(ActionListener mal){
        client.addActionListener(mal);
    }

    public void addProdusListener(ActionListener mal){
        produs.addActionListener(mal);
    }

    public void addOrderListener(ActionListener mal){
        order.addActionListener(mal);
    }
}
