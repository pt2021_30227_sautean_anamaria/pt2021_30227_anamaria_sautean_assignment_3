package start;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProdusBLL;
import model.Client;
import model.Orders;
import presentation.Controller;
import presentation.View;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {

		/*ClientBLL clientBll = new ClientBLL();
		ProdusBLL produsBLL = new ProdusBLL();
        OrderBLL orderBLL = new OrderBLL();
		List<Client> rez = null;
		Client client1 = null;
		try {
			//clientBll.insert(new Client("Test2","test@gmail.com",21));
			//clientBll.update(1245,new Client(1245,"TestUpdate","test@gmail.com",22));
			rez = clientBll.findAll();
			//orderBLL.insert(new Orders(1,1245, 1, 12));
			//client1 = clientBll.findStudentById(1245);
			//rez.forEach(System.out::println);

		} catch (Exception ex) {
			LOGGER.log(Level.INFO, ex.getMessage());
		}

		// obtain field-value pairs for object through reflection
		for(Client c: rez)
			ReflectionExample.retrieveProperties(c);*/
		View view = new View();
		view.setVisible(true);
		view.setSize(750, 250);
		Controller controller = new Controller(view);

	}

}
